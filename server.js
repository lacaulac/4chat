var express = require("express")();
var http = require("http").Server(express);
var io = require("socket.io")(http);

var admin = "lacaulac";

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function getTime()
{
  var now = new Date();
  var h = now.getHours() + 6;
  var m = now.getMinutes();
  var s = now.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  return h + ":" + m + ":" + s;
}
function req(str, repType)
{
  return { question: str, ans: repType };
}

express.get("/", function(req, res){
  console.log("Got a webpage request!");
  res.sendfile(__dirname + "/index.html");
});
express.get("/jquery.js", function(req, res){
  console.log("Got a script request!");
  res.sendfile(__dirname + "/jquery.js");
});
express.get("/ting.ogg", function(req, res){
  console.log("Got a sound request!");
  res.sendfile(__dirname + "/ting.ogg");
});
express.get("/tingco.ogg", function(req, res){
  console.log("Got a sound request!");
  res.sendfile(__dirname + "/tingco.ogg");
});
express.get("/tingdeco.ogg", function(req, res){
  console.log("Got a sound request!");
  res.sendfile(__dirname + "/tingdeco.ogg");
});

io.on("connection", function(socket){
  console.log("A user connected!");
  //socket.emit("request", req("What's your nickname?", "login"));
  socket.on("disconnect", function(){
    console.log("A user disconnected");
  });
  socket.on("message", function(msg){
    if(msg == admin + ": /shutit")
    {
      io.emit("bmessage", "The server asks you politely to shut your mouth.");
    }
    else
    {
      console.log(getTime() + " " + msg);
      socket.broadcast.emit("message", getTime() + " " + msg);
    }
  });
  socket.on("login", function(nick){
    //socket.set("nickname", nick);
    console.log(getTime() + " " + nick + " joined the chat");
    io.emit("userco", getTime() + " " + nick + " joined the chat");
  });
  socket.on("logout", function(nick){
    console.log(getTime() + " " + nick + " leaved the chat");
    io.emit("userdeco", getTime() + " " + nick + " leaved the chat");
  });
});
var port = (process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000);
http.listen(port, (process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1"), function(){
  console.log("Listening on port " + port);
});
